package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TradeTests {
	
	private int testId = 30;
	private String testStock = "AAPL";
	private double testPrice = 13.05;
	private int testVolume = 100;
	
	/**
	 * Test Trade model constructor, availing of getters
	 */
	@Test
	public void test_Trade_Constructor() {
		Trade testTrade = new Trade(testId, testStock, testPrice, testVolume);
		
		assertEquals(testId, testTrade.getId());
		assertEquals(testStock, testTrade.getStock());
		assertEquals(testPrice, testTrade.getPrice(), 0.0001);
		assertEquals(testVolume, testTrade.getVolume());
	}
	
	/**
	 * Test Trade model toString
	 */
	@Test
	public void test_Trade_toString() {
		String testToString = new Trade(testId, testStock, testPrice, testVolume).toString();
		
		assertTrue(testToString.contains((new Integer(testId)).toString()));
		assertTrue(testToString.contains(testStock));
		assertTrue(testToString.contains(String.valueOf(testPrice)));
		assertTrue(testToString.contains(String.valueOf(testVolume)));
	}
}
