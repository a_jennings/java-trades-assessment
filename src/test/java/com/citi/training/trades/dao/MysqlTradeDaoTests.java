package com.citi.training.trades.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradeDaoTests {

	@Autowired
	MysqlTradeDao mysqlTradeDao;
	
	@Test
	@Transactional
	public void test_createAndFindAll() {
		mysqlTradeDao.create(new Trade(-1, "GOOGL", 15.06, 75));
		
		assertTrue(mysqlTradeDao.findAll().size() >= 1);
	}
	
	@Test
	@Transactional
	public void test_createAndFindOne() {
		Trade trade = mysqlTradeDao.create(new Trade(-1, "GOOGL", 15.06, 75));

		assertThat(mysqlTradeDao.findById(trade.getId()).equals(trade));
	}
	
	@Test (expected = TradeNotFoundException.class)
	@Transactional
	public void test_createAndDeleteById() {
		Trade trade = mysqlTradeDao.create(new Trade(-1, "GOOGL", 15.06, 75));
		mysqlTradeDao.deleteById(trade.getId());
		assertEquals(mysqlTradeDao.findById(trade.getId()),1);
	}
}
