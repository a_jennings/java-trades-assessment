# Spring Boot Assessment

A spring-boot REST Interface to create, find all, find by ID and delete elements from a SQL database table called **trade**.

## To run the application

* Right click on the spring-boot app
* Click Run As > Spring Boot App
* Go to http://localhost:8081/